package hotciv;

import hotciv.framework.City;
import hotciv.framework.Player;

public class CityImpl implements City{

    //Variables
    private Player cityOwner; //The owner of the city, Player.RED or Player.BLUE
    private int accumulatedProduction = 0;
    private String unitInProduction;
    private String workForceBalance;

    public CityImpl(Player cityOwner){
        this.cityOwner = cityOwner;
    }

    //Methods
    @Override //Returning the owner of the city
    public Player getOwner() {
        return cityOwner;
    }

    @Override //Returning the size of the city
    public int getSize() {
        return 1;
    }

    @Override //Returns the current production of the city
    public String getProduction() {
        return unitInProduction;
    }

    @Override //Returns the workforce focus of the city eg. food or production
    public String getWorkforceFocus() {
        return workForceBalance;
    }

    //Setting the current city owner
    public void setCityOwner(Player p){
        cityOwner = p;
    }

    //Returns stored production from the city
    public int getAccumulatedProduction(){
        return accumulatedProduction;
    }

    //adds a given amount of production to the city production
    public void addProduction(int i){
        accumulatedProduction += i;
    }

    //Sets the current production of the city eg. LEGION, ARCHER, SETTLER
    public void setProduction(String unitToBeProduced){
        if(unitToBeProduced != null){
            unitInProduction = unitToBeProduced;
        }
    }

    //Sets the workforce balance
    public void setWorkForceBalance(String balance){
        workForceBalance = balance;
    }



}
