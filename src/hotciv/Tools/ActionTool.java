package hotciv.Tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class ActionTool implements Tool {
    private int key;
    private long time;

    private Game game;

    public ActionTool(Game game) {
        this.game = game;
    }


    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        if(System.currentTimeMillis() < time+1000 && key==16){
            Position gamePos = GfxConstants.getPositionFromXY(i,i1);

            if(game.getUnitAt(gamePos) != null){
                System.out.println("Performing Action at " + GfxConstants.getPositionFromXY(i,i1));
                game.performUnitActionAt(GfxConstants.getPositionFromXY(i,i1));
            }


        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        key=keyEvent.getKeyCode();
        time=System.currentTimeMillis();


    }
}
