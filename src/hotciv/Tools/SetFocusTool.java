package hotciv.Tools;

import hotciv.framework.Game;
import hotciv.view.GfxConstants;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class SetFocusTool implements Tool {

    Game game;
    public SetFocusTool(Game game) {
        this.game = game;
    }




    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        game.setTileFocus(GfxConstants.getPositionFromXY(i,i1));
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {

    }
}
