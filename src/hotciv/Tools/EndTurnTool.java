package hotciv.Tools;

import hotciv.framework.Game;
import hotciv.view.GfxConstants;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class EndTurnTool implements Tool{
    Game game;

    public EndTurnTool(Game game){
        this.game=game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        if(mouseEvent.getX() >= GfxConstants.TURN_SHIELD_X && mouseEvent.getX() <= GfxConstants.TURN_SHIELD_X + 30 && mouseEvent.getY() >= GfxConstants.TURN_SHIELD_Y && mouseEvent.getY() <= GfxConstants.TURN_SHIELD_Y + 40){
            game.endOfTurn();
        }
        //System.out.println("Gfxconstants    X:"+GfxConstants.TURN_SHIELD_X + "    Y: "+GfxConstants.TURN_SHIELD_Y);
        //System.out.println("Mouseinput      X:"+mouseEvent.getX()+"     Y: "+mouseEvent.getY());

    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {

    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {

    }
}
