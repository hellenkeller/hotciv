package hotciv.Tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;
import minidraw.standard.SelectionTool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class UnitMoveTool implements Tool {

    private Game game;
    private Position startPos;
    private SelectionTool selectionTool;

    public UnitMoveTool(Game g, DrawingEditor editor){
        game=g;
        this.selectionTool = new SelectionTool(editor);
    }


    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        if(i <= 500){
            selectionTool.mouseDown(mouseEvent,i,i1);
        }

    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        if(i <= 500){
            selectionTool.mouseDrag(mouseEvent,i,i1);
        }
        if(startPos == null){
            startPos = GfxConstants.getPositionFromXY(i,i1);

        }
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        if(i <= 500){
            selectionTool.mouseUp(mouseEvent,i,i1);
        }
        if(startPos != null){
            System.out.println("startPos " + startPos + " endPos: " + GfxConstants.getPositionFromXY(i,i1));
            game.moveUnit(startPos,GfxConstants.getPositionFromXY(i,i1));
            startPos=null;
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {
        if(i <= 500){
            selectionTool.mouseMove(mouseEvent,i,i1);
        }
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        if(i <= 500){
            selectionTool.keyDown(keyEvent,i);
        }
    }
}
