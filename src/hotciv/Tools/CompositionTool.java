package hotciv.Tools;

import hotciv.framework.Game;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class CompositionTool implements Tool {
    private Tool actionTool;
    private Tool endTurnTool;
    private Tool moveTool;
    private Tool setFocusTool;


    public CompositionTool(Game game, DrawingEditor editor) {
        this.actionTool = new ActionTool(game);
        this.endTurnTool = new EndTurnTool(game);
        this.moveTool = new UnitMoveTool(game,editor);
        this.setFocusTool = new SetFocusTool(game);
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        System.out.println(i+"  "+i1);
        actionTool.mouseDown(mouseEvent,i,i1);
        endTurnTool.mouseDown(mouseEvent,i,i1);
        moveTool.mouseDown(mouseEvent,i,i1);
        setFocusTool.mouseDown(mouseEvent,i,i1);
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        actionTool.mouseDrag(mouseEvent,i,i1);
        endTurnTool.mouseDrag(mouseEvent,i,i1);
        moveTool.mouseDrag(mouseEvent,i,i1);
        setFocusTool.mouseDrag(mouseEvent,i,i1);
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        actionTool.mouseUp(mouseEvent,i,i1);
        endTurnTool.mouseUp(mouseEvent,i,i1);
        moveTool.mouseUp(mouseEvent,i,i1);
        setFocusTool.mouseUp(mouseEvent,i,i1);
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {
        actionTool.mouseMove(mouseEvent,i,i1);
        endTurnTool.mouseMove(mouseEvent,i,i1);
        moveTool.mouseMove(mouseEvent,i,i1);
        setFocusTool.mouseMove(mouseEvent,i,i1);
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        actionTool.keyDown(keyEvent,i);
        endTurnTool.keyDown(keyEvent,i);
        moveTool.keyDown(keyEvent,i);
        setFocusTool.keyDown(keyEvent,i);
    }
}
