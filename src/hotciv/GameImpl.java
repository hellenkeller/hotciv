package hotciv;

import hotciv.Observers.AttackWinnerObserver;
import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.*;
import hotciv.gameTypeFactory.GameTypeFactory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Skeleton implementation of HotCiv.
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class GameImpl implements Game {
    //HashMaps for locations
    //We have created hashMaps, to control the Positions of all our units, cities and tiles
    private HashMap<Position, City> cities = new HashMap<>();
    private HashMap<Position, Tile> tiles = new HashMap<>();
    private HashMap<Position, Unit> units = new HashMap<>();
    private ArrayList<GameObserver> gameObservers = new ArrayList<>();

    private ArrayList<AttackWinnerObserver> attackWinnerObservers = new ArrayList<>();

    //Variables
    private Player playerInTurn = Player.RED; //Value contains the player in turn
    private int gameAge = -4000;
    private int roundCount = 0;
    private WinnerStrategy winnerStrategy;
    private AgeingStrategy ageingStrategy;
    private UnitActionStrategy unitActionStrategy;
    private WorldBuildingStrategy worldBuildingStrategy;
    private AttackingStrategy attackingStrategy;

    public GameImpl(GameTypeFactory gameTypeFactory) {
        this.winnerStrategy = gameTypeFactory.createWinnerStrategy(this);
        this.ageingStrategy = gameTypeFactory.createAgeingStrategy();
        this.unitActionStrategy = gameTypeFactory.createUnitActionStrategy();
        this.worldBuildingStrategy = gameTypeFactory.createWorldBuildingStrategy();
        this.attackingStrategy = gameTypeFactory.createAttackingStrategy(this);

        createWorld();
    }

    //Getting tile at requested position
    public Tile getTileAt(Position p) {
        return tiles.get(p);
    }

    //Getting unit at requested position
    public Unit getUnitAt(Position p) {
        return units.get(p);
    }

    //Getting city at requested position
    public City getCityAt(Position p) {
        return cities.get(p);
    }

    //Returning the player
    public Player getPlayerInTurn() {
        return playerInTurn;
    }

    //We know that this is fake it code, but it didn't make sense to us to implement an actual method at this point.
    public Player getWinner() {
        return winnerStrategy.getWinner(this);
    }

    //Moves a unit if possible. Returns true if move succeeded, and false if not.
    public boolean moveUnit(Position posFrom, Position posTo) {
        float distance = getLength(posFrom,posTo);

        if (getUnitAt(posFrom) == null) {
            return false;
        }
        if (getUnitAt(posFrom).getOwner() != playerInTurn) {
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);
            return false;
        }
        if (getUnitAt(posFrom).getMoveCount() < 1) {
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);
            return false;
        }
        if (!checkMove(posFrom, posTo)) {
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);
            return false;
        }
        if (!((TileImpl) getTileAt(posTo)).getMovementAllowed(getUnitAt(posFrom))) {
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);
            return false;
        }
        if (((int) distance) > getUnitAt(posFrom).getMoveCount()) {
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);
            return false;
        }

        checkTileForCity(posFrom, posTo);

        Unit unitToBeMoved = getUnitAt(posFrom);
        units.remove(posFrom);
        units.put(posTo, unitToBeMoved);

        Unit unit = getUnitAt(posTo);
        ((UnitImpl) unit).setMoveCount(unit.getMoveCount() - ((int)distance)); //sets player movement to 0, so that the unit cannot move until next turn

        notifyWorldChange(posFrom);
        notifyWorldChange(posTo);
        notifyTileFocusChange(posFrom);
        notifyTileFocusChange(posTo);
        return true;
    }

    //Returns the game age, where BC3000 will be returned as -3000, and 3000 will simply return as 3000
    public int getAge() {
        return gameAge;
    }

    public int getRoundCount() {
        return roundCount;
    }

    //This will advance the gameAge, and change turn to the other player
    public void endOfTurn() {
        getWinner();
        switch (getPlayerInTurn()) {
            case RED:
                playerInTurn = Player.BLUE;
                break;
            case BLUE:
                playerInTurn = Player.RED;

                //Round actions
                roundCount++;
                gameAge = ageingStrategy.setGameAge(getAge());
                resetMoveCount();   //Restored mobility to all units, by setting their moveCount to 1, granting them one move for this turn
                performCityDuties();    //Adds 6 production to the city, and buys the unit in production if possible
                break;
        }
        notifyTurnEnd(playerInTurn,gameAge);
    }

    //Changes the workForce Focus in a city
    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        City city = getCityAt(p);
        ((CityImpl) city).setWorkForceBalance(balance);
    }

    //Changes the Production in a city
    public void changeProductionInCityAt(Position p, String unitType) {
        City city = getCityAt(p);
        ((CityImpl) city).setProduction(unitType);
    }

    //Performs the unit action.
    public void performUnitActionAt(Position p) {
        if (getUnitAt(p).getOwner() != getPlayerInTurn()) {
            return;
        }
        unitActionStrategy.performUnitAction(this, p);
        notifyWorldChange(p);
    }

    @Override
    public void addObserver(GameObserver observer) {
        gameObservers.add(observer);
    }

    public void notifyWorldChange(Position p){
        for(GameObserver g:gameObservers){
            g.worldChangedAt(p);
        }
    }

    private void notifyTurnEnd(Player nextPlayer, int age){
        for(GameObserver g:gameObservers){
            g.turnEnds(nextPlayer,age);
        }
    }

    private void notifyTileFocusChange(Position p){
        for(GameObserver g:gameObservers){
            g.tileFocusChangedAt(p);
        }
    }

    @Override
    public void setTileFocus(Position position) {
        notifyTileFocusChange(position);
    }

    public HashMap<Position, City> getCities() {
        return cities;
    }

    //Places new tile, overwrites old tile if any
    public void placeTile(Position p, String type) {
        tiles.put(p, new TileImpl(type));
    }

    //build new city
    public void buildNewCity(Position p, Player owner) {
        cities.put(p, new CityImpl(owner));
    }

    //Build and produce units
    public boolean produceUnit(Position p, Player owner, String type) {
        Position spawnPosition = checkProductionSpawn(p, type);
        if (spawnPosition == null) {
            return false;
        }
        units.put(spawnPosition, new UnitImpl(type, owner));
        return true;
    }

    public void removeUnit(Position p) {
        units.remove(p);
    }

    void createTile(Position p, String type) {
        Tile tile = new TileImpl(type);
        tiles.put(p, tile);
    }

    //Sets moveCount of all units to 1
    private void resetMoveCount() {
        units.forEach((pos, unit) -> {
            if (unit.getMoveCount() == -1) {
                return;
            }
            ((UnitImpl) unit).resetMoveCount();
            notifyWorldChange(pos);
        });
    }

    //Performs city actions such as adding 6 production, and buying units once enough production is accumulated
    private void performCityDuties() {
        cities.forEach((pos, city) -> {
            ((CityImpl) city).addProduction(6);
            String unit = city.getProduction();

            if (unit != null) {
                int price = new UnitImpl(unit, city.getOwner()).getPrice();
                if (((CityImpl) city).getAccumulatedProduction() >= price) {
                    produceUnit(pos, city.getOwner(), unit);
                    ((CityImpl) city).addProduction(-price);
                }
            }
        });

    }

    //Placing predefined units, cities and tile types arround the map.
    private void createWorld() {
        worldBuildingStrategy.createWorld(this);
    }

    //Returns the length between the two given points
    private float getLength(Position posFrom, Position posTo) {
        return Math.max(Math.abs(posFrom.getColumn() - posTo.getColumn()), Math.abs(posFrom.getRow() - posTo.getRow()));
    }

    //Checks if the move is within the confines of the map, and if there is any players in the tile. If so it handles it.
    //Checks if there is any city on the tile.
    //Checking if the spawn location is occupied, and supply a new spawn position if so
    private boolean checkMove(Position posFrom, Position posTo) { //Position From and To.
        if (posTo.getRow() < 0 || posTo.getRow() > 15 || posTo.getColumn() < 0 || posTo.getColumn() > 15 || getUnitAt(posFrom) == null) {
            return false;
        }
        if (getUnitAt(posTo) == null) {
            return true;
        } else {
            Player occupant = getUnitAt(posTo).getOwner();
            notifyWorldChange(posFrom);
            notifyWorldChange(posTo);

            if (occupant != playerInTurn) {
                updateAttackWinnerObservers();
                if(attackingStrategy.attack(this, posFrom, posTo)){
                    return true;
                }
            }
        }

        return false;
    }

    private void checkTileForCity(Position posFrom, Position posTo) {
        City cityToMoveTo = getCityAt(posTo);
        Unit unitMoving = getUnitAt(posFrom);

        if (cityToMoveTo != null) {
            if (cityToMoveTo.getOwner() == playerInTurn) {
                ((UnitImpl) unitMoving).setDefendedCity(cityToMoveTo);
            }
            if (cityToMoveTo.getOwner() != playerInTurn) {
                ((CityImpl) cityToMoveTo).setCityOwner(playerInTurn);
                ((UnitImpl) unitMoving).setDefendedCity(cityToMoveTo);
            }
        }
        if (getCityAt(posFrom) != null && getCityAt(posTo) == null) {
            ((UnitImpl) getUnitAt(posFrom)).setDefendedCity(null);
        }
    }

    public ArrayList<Position> getSurroundingPositions(Position p) {
        ArrayList<Position> positions = new ArrayList<>();

        positions.add(p);
        positions.add(new Position(p.getRow() - 1, p.getColumn()));
        positions.add(new Position(p.getRow() - 1, p.getColumn() + 1));
        positions.add(new Position(p.getRow(), p.getColumn() + 1));
        positions.add(new Position(p.getRow() + 1, p.getColumn() + 1));
        positions.add(new Position(p.getRow() + 1, p.getColumn()));
        positions.add(new Position(p.getRow() + 1, p.getColumn() - 1));
        positions.add(new Position(p.getRow(), p.getColumn() - 1));
        positions.add(new Position(p.getRow() - 1, p.getColumn() - 1));

        ArrayList<Position> positionList = new ArrayList<>();
        for (Position pos : positions) {
            if (pos.getColumn() >= 0 && pos.getColumn() < 16 && pos.getRow() >= 0 && pos.getRow() < 16) {
                positionList.add(pos);
            }
        }

        return positionList;
    }

    private Position checkProductionSpawn(Position cityPos, String type) {
        for (Position p : getSurroundingPositions(cityPos)) {
            if (p.getColumn() < 0 || p.getRow() < 0) {
                continue;
            }
            if (getUnitAt(p) == null) {
                if (((TileImpl) getTileAt(p)).getMovementAllowed(new UnitImpl(type, Player.RED)) && getUnitAt(p) == null) {
                    return p;
                }
            }
        }
        return null;
    }

    public void addAttackObserver(AttackWinnerObserver attackWinnerObserver){
        attackWinnerObservers.add(attackWinnerObserver);
    }

    private void updateAttackWinnerObservers(){
        for(AttackWinnerObserver awo:attackWinnerObservers){
            awo.addWin(getPlayerInTurn());
        }
    }
}

