package hotciv;

import hotciv.framework.*;

import java.io.PrintStream;

public class TranscriptDecorator implements Game {

    private PrintStream log;
    Game game;

    TranscriptDecorator(Game game, PrintStream log){
        this.log = log;
        this.game = game;
    }

    @Override
    public Tile getTileAt(Position p) {
        return game.getTileAt(p);
    }

    @Override
    public Unit getUnitAt(Position p) {
        return game.getUnitAt(p);
    }

    @Override
    public City getCityAt(Position p) {
        return game.getCityAt(p);
    }

    @Override
    public Player getPlayerInTurn() {
        return game.getPlayerInTurn();
    }

    @Override
    public Player getWinner() {
        return game.getWinner();
    }

    @Override
    public int getAge() {
        return game.getAge();
    }

    @Override
    public boolean moveUnit(Position from, Position to) {
        Unit unit = getUnitAt(from);
        boolean bool = game.moveUnit(from,to);
        if(bool){
            log.println(getPlayerInTurn() + " moves " + unit.getTypeString() + " from " + from + " to " + to);
        }
        return bool;
    }

    @Override
    public void endOfTurn() {
        log.println(getPlayerInTurn() + " ends turn.");
        game.endOfTurn();
    }

    @Override
    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        log.println(getPlayerInTurn() + " changes work force fokus in his city at " + p + " to " + balance);
        game.changeWorkForceFocusInCityAt(p,balance);
    }

    @Override
    public void changeProductionInCityAt(Position p, String unitType) {
        log.println(getPlayerInTurn() + " changes production in a city at " + p + " to " + unitType);
        game.changeProductionInCityAt(p,unitType);

    }

    @Override
    public void performUnitActionAt(Position p) {
        log.println(getPlayerInTurn() + " performs unit action at unit on an " + getUnitAt(p).getTypeString() + " at position " + p);
        game.performUnitActionAt(p);

    }

    @Override
    public void addObserver(GameObserver observer) {

    }

    @Override
    public void setTileFocus(Position position) {

    }

}
