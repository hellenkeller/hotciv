package hotciv.Observers;

import hotciv.framework.Player;

import java.util.HashMap;

public interface AttackWinnerObserver {
    /**
     * @param player is the player who won the attack. This will count one extra successful attack
     */
    public HashMap<Player,Integer> attacks = new HashMap<>();
    public void addWin(Player player);
}
