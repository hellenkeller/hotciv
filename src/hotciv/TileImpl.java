package hotciv;

import hotciv.framework.GameConstants;
import hotciv.framework.Tile;
import hotciv.framework.Unit;

public class TileImpl implements Tile {

    private String type; //Type of tile, e.g PLAINS, MOUNTAINS, OCEAN, HILLS

    public TileImpl(String type){
        this.type = type;
    }

    @Override //Returning the type
    public String getTypeString() {
        return type;
    }

    //Returns the amount a tile produces (Does not return what the tile produce, that is done with the getProductionType method)
    int getProductionAmount() {
        int returnInt = 0; //The Integer that is to be returned with the defensive value
        switch (getTypeString()){
            case GameConstants.MOUNTAINS:
                returnInt = 1;
                break;
            case GameConstants.PLAINS:
                returnInt = 3;
                break;
            case GameConstants.HILLS:
                returnInt = 2;
                break;
            case GameConstants.OCEANS:
                returnInt = 1;
                break;
            case GameConstants.FOREST:
                returnInt = 3;
                break;
        }
        return returnInt;
    }

    //Returns the type of product a tile produces (Does not return the amount the tile produce, that is done with the getProductionAmount method)
    String getProductionType() {
        String returnString = null; //The Integer that is to be returned with the defensive value
        switch (getTypeString()){
            case GameConstants.MOUNTAINS:
                returnString = "Production";
                break;
            case GameConstants.PLAINS:
                returnString = "Food";
                break;
            case GameConstants.HILLS:
                returnString = "Production";
                break;
            case GameConstants.OCEANS:
                returnString = "Food";
                break;
            case GameConstants.FOREST:
                returnString = "Production";
                break;
        }
        return returnString;
    }

    boolean getMovementAllowed(Unit unit){

        switch (((UnitImpl)unit).getUnitElement()){
            case LAND:
                if(type.equals(GameConstants.MOUNTAINS) || type.equals(GameConstants.OCEANS)){
                    return false;
                }
                break;
            case WATER:
                if(!type.equals(GameConstants.OCEANS)){
                    return false;
                }
                break;
        }
        return true;
    }
}
