package hotciv.Strategies.WinnerStrategy;

import hotciv.GameImpl;
import hotciv.Observers.AttackWinnerObserver;
import hotciv.framework.Game;
import hotciv.framework.Player;


public class EpsilonCivWinnerStrategy implements WinnerStrategy, AttackWinnerObserver {

    public EpsilonCivWinnerStrategy(Game game){
        ((GameImpl)game).addAttackObserver(this);
        attacks.clear();
    }

    @Override
    public Player getWinner(Game game) {
        if (attacks.get(game.getPlayerInTurn()) != null && attacks.get(game.getPlayerInTurn()) >= 3) {
            return game.getPlayerInTurn();
        }

        return null;
    }

    @Override
    public void addWin(Player player) {
        attacks.putIfAbsent(player,0);
        int playerAttackCount = attacks.get(player);

        attacks.replace(player,playerAttackCount+1);
    }
}