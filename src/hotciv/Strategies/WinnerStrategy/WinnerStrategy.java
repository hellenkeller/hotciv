package hotciv.Strategies.WinnerStrategy;

import hotciv.framework.Game;
import hotciv.framework.Player;

public interface WinnerStrategy {
    /**
     * @return the winner of the game if there is any
     * @param game game, for assessing the winner of the game.
     */



    public Player getWinner(Game game);

}
