package hotciv.Strategies.WinnerStrategy;

import hotciv.framework.Game;
import hotciv.framework.Player;

public class AlphaCivWinnerStrategy implements WinnerStrategy{

    @Override
    public Player getWinner(Game game) {

        //Red will win in 3000BC, there is no other options
        if(game.getAge() == -3000){
            return Player.RED;
        }
        return null;
    }
}
