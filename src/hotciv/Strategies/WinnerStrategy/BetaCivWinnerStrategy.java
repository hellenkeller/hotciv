package hotciv.Strategies.WinnerStrategy;

import hotciv.GameImpl;
import hotciv.framework.City;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;

import java.util.Map;

/**
 * When i player have occupied and have ownership of all cities, the player wins
 */

public class BetaCivWinnerStrategy implements WinnerStrategy {
    @Override
    public Player getWinner(Game game) {
        for(Map.Entry<Position, City> entry : ((GameImpl) game).getCities().entrySet()) {
            City city = entry.getValue();

            if(city.getOwner() != game.getPlayerInTurn()) {
                return null;
            }
        }
        return game.getPlayerInTurn();
    }
}

