package hotciv.Strategies.WinnerStrategy;

import hotciv.framework.Game;
import hotciv.framework.Player;

public class TestWinnerStrategy implements WinnerStrategy {

    //This implementation have been made for testing purpose only. It is only used when the testing doesn't require an winner strategy
    @Override
    public Player getWinner(Game game) {
        return null;
    }
}
