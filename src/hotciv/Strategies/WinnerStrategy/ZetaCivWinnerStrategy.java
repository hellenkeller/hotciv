package hotciv.Strategies.WinnerStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.Player;

public class ZetaCivWinnerStrategy implements WinnerStrategy{

    private EpsilonCivWinnerStrategy epsilonCivWinnerStrategy;

    @Override
    public Player getWinner(Game game) {
        //After 20 rounds, the epsilonWinnerStrategy is created, and from there it will start counting wins
        if (((GameImpl)game).getRoundCount() == 20){
            epsilonCivWinnerStrategy = new EpsilonCivWinnerStrategy(game);
        }

        if(((GameImpl)game).getRoundCount() <= 20){
            return new BetaCivWinnerStrategy().getWinner(game);
        }
        else{
            return epsilonCivWinnerStrategy.getWinner(game);
        }
    }
}
