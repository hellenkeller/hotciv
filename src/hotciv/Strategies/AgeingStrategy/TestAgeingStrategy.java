package hotciv.Strategies.AgeingStrategy;

public class TestAgeingStrategy implements AgeingStrategy {

    //This implementation have been made for testing purpose only. It is used when the testing doesn't require an ageing strategy

    @Override
    public int setGameAge(int currentGameAge) {
        return 0;
    }
}
