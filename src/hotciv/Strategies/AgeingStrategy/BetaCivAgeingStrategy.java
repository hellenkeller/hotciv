package hotciv.Strategies.AgeingStrategy;

public class BetaCivAgeingStrategy implements AgeingStrategy {

    @Override
    public int setGameAge(int currentGameAge) {

        //All the if statements covers the specification from 36.3.1 in the book
        if(currentGameAge >= -4000 && currentGameAge < -100){ return currentGameAge +100; }
        else if(currentGameAge == -100){ return -1; }
        else if(currentGameAge == -1){ return 1; }
        else if(currentGameAge == 1){ return 50; }
        else if(currentGameAge >= 50 && currentGameAge < 1750){ return currentGameAge +50; }
        else if(currentGameAge >= 1750 && currentGameAge < 1900){ return currentGameAge +25; }
        else if(currentGameAge >= 1900 && currentGameAge < 1970){ return currentGameAge +5; }
        else if(currentGameAge >= 1970){ return currentGameAge +1; }
        return 0;

    }
}
