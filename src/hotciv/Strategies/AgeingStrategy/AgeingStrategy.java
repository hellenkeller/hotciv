package hotciv.Strategies.AgeingStrategy;

public interface AgeingStrategy {
    /**
     * This framework defines how the game will age.
     * @return the age of the current game.
     * @param currentGameAge the current age of the game
     */
    public int setGameAge(int currentGameAge);



}
