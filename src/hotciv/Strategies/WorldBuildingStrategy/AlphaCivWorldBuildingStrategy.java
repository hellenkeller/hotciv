package hotciv.Strategies.WorldBuildingStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;

public class AlphaCivWorldBuildingStrategy implements WorldBuildingStrategy {
    @Override
    public void createWorld(Game game) {
        for(int i = 0; i < GameConstants.WORLDSIZE; i++) {
            for(int j = 0; j<GameConstants.WORLDSIZE; j++){
                ((GameImpl)game).placeTile(new Position(i,j),GameConstants.PLAINS);
            }
        }
        ((GameImpl)game).buildNewCity(new Position(1,1),Player.RED);
        ((GameImpl)game).buildNewCity(new Position(4,1),Player.BLUE);
        ((GameImpl)game).produceUnit(new Position(2,0),Player.RED,GameConstants.ARCHER);
        ((GameImpl)game).produceUnit(new Position(3,2),Player.BLUE,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(4,3),Player.RED,GameConstants.SETTLER);

        ((GameImpl)game).placeTile(new Position(1,0),GameConstants.OCEANS);
        ((GameImpl)game).placeTile(new Position(0,1),GameConstants.HILLS);
        ((GameImpl)game).placeTile(new Position(2,2),GameConstants.MOUNTAINS);
    }
}
