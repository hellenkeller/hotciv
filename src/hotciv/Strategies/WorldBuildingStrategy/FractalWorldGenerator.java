/*package hotciv.Strategies.WorldBuildingStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import thirdparty.ThirdPartyFractalGenerator;
//import thirdparty.ThirdPartyFractalGenerator;

public class FractalWorldGenerator implements WorldBuildingStrategy {

    @Override
    public void createWorld(Game game) {
        ThirdPartyFractalGenerator generator = new ThirdPartyFractalGenerator();

        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {

                char tileChar = generator.getLandscapeAt(r,c);
                String type = "error";

                if (tileChar == '.') {
                    type = GameConstants.OCEANS;
                }
                if (tileChar == 'o') {
                    type = GameConstants.PLAINS;
                }
                if (tileChar == 'M') {
                    type = GameConstants.MOUNTAINS;
                }
                if (tileChar == 'f') {
                    type = GameConstants.FOREST;
                }
                if (tileChar == 'h') {
                    type = GameConstants.HILLS;
                }

                Position p = new Position(r, c);
                ((GameImpl)game).placeTile(p,type);
            }
        }
    }
}*/
