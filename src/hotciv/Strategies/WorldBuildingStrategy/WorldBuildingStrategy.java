package hotciv.Strategies.WorldBuildingStrategy;

import hotciv.framework.Game;

public interface WorldBuildingStrategy {

    /**
     * Creates a game world, and places units
     * and cities within it
     */

    public void createWorld(Game game);
}
