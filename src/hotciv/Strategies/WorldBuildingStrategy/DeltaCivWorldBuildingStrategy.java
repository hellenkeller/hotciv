package hotciv.Strategies.WorldBuildingStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;

public class DeltaCivWorldBuildingStrategy implements WorldBuildingStrategy {
    @Override
    public void createWorld(Game game) {
        /**
         * Building the world
         * This code have been taken from the course website, and modified to our needs
         */
        Game theGame = game;
        // Basically we use a 'data driven' approach - code the
        // worldLayout in a simple semi-visual representation, and
        // convert it to the actual Game representation.

        //Tiles
        String[] worldLayout =
                new String[]{
                        "...ooMooooo.....",
                        "..ohhoooofffoo..",
                        ".oooooMooo...oo.",
                        ".ooMMMoooo..oooo",
                        "...ofooohhoooo..",
                        ".ofoofooooohhoo.",
                        "...ooo..........",
                        ".ooooo.ooohooM..",
                        ".ooooo.oohooof..",
                        "offfoooo.offoooo",
                        "oooooooo...ooooo",
                        ".ooMMMoooo......",
                        "..ooooooffoooo..",
                        "....ooooooooo...",
                        "..ooohhoo.......",
                        ".....ooooooooo..",
                };

        //Units
        //archer red    =   a
        //archer blue   =   b
        //legion red    =   c
        //legion blue   =   d
        //settler red   =   e
        //settler blue  =   f
        //galley red    =   g
        //galley blue   =   h
        String[] unitPositions =
                new String[]{
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxaxxxxxxx",
                        "xxxxdxxxxxxxxxxx",
                        "xxxxxexxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                };

        //Cities
        //Red City  =   r
        //Blue City =   b
        String[] cityPositions =
                new String[]{
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxbxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxrxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                        "xxxxxxxxxxxxxxxx",
                };
        // Converting the worldLayout to positions and placing them
        String tileLine;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            tileLine = worldLayout[r];
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                char tileChar = tileLine.charAt(c);
                String type = "error";
                if (tileChar == '.') {
                    type = GameConstants.OCEANS;
                }
                if (tileChar == 'o') {
                    type = GameConstants.PLAINS;
                }
                if (tileChar == 'M') {
                    type = GameConstants.MOUNTAINS;
                }
                if (tileChar == 'f') {
                    type = GameConstants.FOREST;
                }
                if (tileChar == 'h') {
                    type = GameConstants.HILLS;
                }
                Position p = new Position(r, c);
                ((GameImpl)theGame).placeTile(p,type);
            }
        }

        //Converting the unitPositions and placing them in the world
        String unitLine;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            unitLine = unitPositions[r];
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                char unitChar = unitLine.charAt(c);
                if (unitChar == 'a') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.RED,GameConstants.ARCHER);
                }
                if (unitChar == 'b') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.BLUE,GameConstants.ARCHER);
                }
                if (unitChar == 'c') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.RED,GameConstants.LEGION);
                }
                if (unitChar == 'd') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.BLUE,GameConstants.LEGION);
                }
                if (unitChar == 'e') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.RED,GameConstants.SETTLER);
                }
                if (unitChar == 'f') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.BLUE,GameConstants.SETTLER);
                }
                if (unitChar == 'g') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.RED,GameConstants.GALLEY);
                }
                if (unitChar == 'h') {
                    ((GameImpl)game).produceUnit(new Position(r,c),Player.BLUE,GameConstants.GALLEY);
                }
            }
        }

        //Converting the cityPositions and placing them in the world
        String cityLine;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            cityLine = cityPositions[r];
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                char cityChar = cityLine.charAt(c);
                if (cityChar == 'r') {
                    ((GameImpl)game).buildNewCity(new Position(r,c),Player.RED);
                }
                if (cityChar == 'b') {
                    ((GameImpl)game).buildNewCity(new Position(r,c),Player.BLUE);
                }
            }
        }
    }
}
