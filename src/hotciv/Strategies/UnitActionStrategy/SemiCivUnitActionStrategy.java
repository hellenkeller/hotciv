package hotciv.Strategies.UnitActionStrategy;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;

public class SemiCivUnitActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitAction(Game game, Position p) {

        if(game.getUnitAt(p) == null){
            return;
        }
        if(game.getUnitAt(p).getTypeString().equals(GameConstants.SETTLER)){
            new GammaCivUnitActionStrategy().performSettlerAction(game,p);
        }

    }
}
