package hotciv.Strategies.UnitActionStrategy;

import hotciv.GameImpl;
import hotciv.UnitImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.framework.Unit;

public class GammaCivUnitActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitAction(Game game, Position p) {
        Unit unit = game.getUnitAt(p);
        switch (unit.getTypeString()){
                case GameConstants.ARCHER:
                    performArcherAction(unit);
                    break;
                case GameConstants.SETTLER:
                    performSettlerAction(game,p);
                    break;
            }
        }
        private void performArcherAction(Unit unit){
            if(unit.getMoveCount() == -1){
                ((UnitImpl)unit).setDefensiveStrength(unit.getDefensiveStrength()/2);
                ((UnitImpl)unit).setMoveCount(1);
            }else{
                ((UnitImpl)unit).setDefensiveStrength(unit.getDefensiveStrength()*2);
                ((UnitImpl)unit).setMoveCount(-1);  //By setting the moveCount to -1, i am signaling both that this unit have used it's action, and that it cannot move
            }
        }
        void performSettlerAction(Game game, Position p){
            ((GameImpl)game).buildNewCity(p,game.getUnitAt(p).getOwner());
            ((GameImpl)game).removeUnit(p);
        }
    }
