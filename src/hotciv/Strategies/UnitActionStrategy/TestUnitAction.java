package hotciv.Strategies.UnitActionStrategy;

import hotciv.framework.Game;
import hotciv.framework.Position;

public class TestUnitAction implements UnitActionStrategy {


    //This implementation have been made for testing purpose only. It is used when the testing doesn't require an unit action strategy
    @Override
    public void performUnitAction(Game game, Position p) {

    }
}
