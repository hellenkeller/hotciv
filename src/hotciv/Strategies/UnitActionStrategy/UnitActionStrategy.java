package hotciv.Strategies.UnitActionStrategy;

import hotciv.framework.Game;
import hotciv.framework.Position;

public interface UnitActionStrategy {
    /**
     * This method will perform the given action that is assigned to a unit
     * For settler, this action is basically replacing the settler with a city of population 1
     * For archer, this is boosting the attack strength and disabling movement
     * @param game is the game in within we need to perform the unit action
     * @param p is the position of the unit
     */
    public void performUnitAction(Game game, Position p);
}
