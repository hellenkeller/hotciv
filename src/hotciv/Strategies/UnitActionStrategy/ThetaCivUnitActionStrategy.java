package hotciv.Strategies.UnitActionStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.ThetaCivFactory;


public class ThetaCivUnitActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitAction(Game game, Position p) {
        if (game.getUnitAt(p) == null || !game.getUnitAt(p).getTypeString().equals(GameConstants.GALLEY)) {
            return;
        }

        for (Position pos : new GameImpl(new ThetaCivFactory()).getSurroundingPositions(p)) {
            if (((GameImpl) game).produceUnit(p, game.getUnitAt(p).getOwner(), GameConstants.SETTLER)) {
                ((GameImpl) game).removeUnit(pos);
                return;
            }
        }
    }
}
