package hotciv.Strategies.RandomNumberStrategy;

import java.util.Random;

public class RandomNumbers implements RandomNumberStrategy {
    @Override
    public int[] generateNumbers() {

        int[] numbers = new int[2];

        Random rand1 = new Random();
        int attackRandom = rand1.nextInt(5) + 1;

        Random rand2 = new Random();
        int defenseRandom = rand2.nextInt(5) + 1;

        numbers[0] = attackRandom;
        numbers[1] = defenseRandom;

        return numbers;
    }
}
