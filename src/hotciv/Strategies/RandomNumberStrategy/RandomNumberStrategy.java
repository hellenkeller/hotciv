package hotciv.Strategies.RandomNumberStrategy;

public interface RandomNumberStrategy {
    /**
     * @return returns two random numbers in an int array ( int[0] and int [1] )
     */

    public int[] generateNumbers();
}
