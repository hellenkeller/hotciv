package hotciv.Strategies.AttackingStrategy;

import hotciv.GameImpl;
import hotciv.Strategies.RandomNumberStrategy.RandomNumberStrategy;
import hotciv.framework.*;
import hotciv.gameTypeFactory.AlphaCivFactory;

import java.util.ArrayList;

public class EpsilonCivAttackStrategy implements AttackingStrategy {

    private int[] randNumbers;

    public EpsilonCivAttackStrategy(RandomNumberStrategy rns) {

        this.randNumbers = rns.generateNumbers();

    }

    @Override
    public boolean attack(Game game, Position from, Position to) {
        //Calculating the attacking strength of the attacking unit and the defensive strength of the unit under attack
        int attackStrength = calculateAttackingStrength(game, from);
        int defensiveStrength = calculateDefensiveStrength(game, to);

        if (attackStrength > defensiveStrength) {
            ((GameImpl) game).removeUnit(to);
            //countSuccessfulAttack(game);
            return true;
        } else {
            ((GameImpl) game).removeUnit(from);
            ((GameImpl) game).notifyWorldChange(from);
            return false;
        }
    }
    public int calculateAttackingStrength(Game game, Position unitPos) {
        //Checking for Tile or city
        int attackMultiplier = getMultiplier(game, unitPos);

        //Checking adjacent tiles for friendly units
        int additionalAttackStrength = getNrOfNearbyFriendlyUnits(game, unitPos);

        int newAttackStrength = game.getUnitAt(unitPos).getAttackingStrength() + additionalAttackStrength;
        newAttackStrength = newAttackStrength * attackMultiplier * randNumbers[0];

        System.out.println(game.getUnitAt(unitPos).getTypeString() + " attacks: " + "standAttack: " + game.getUnitAt(unitPos).getAttackingStrength() + "  newAttack: " + newAttackStrength + "   multiplier: "+attackMultiplier + "    additionalDef: " + additionalAttackStrength + "  rand: " + randNumbers[0]);

        return newAttackStrength;
    }
    public int calculateDefensiveStrength(Game game, Position unitPos) {

        //Checking for Tile or city
        int defenceMultiplier = getMultiplier(game, unitPos);

        int additionalDefensiveStrength = getNrOfNearbyFriendlyUnits(game, unitPos);
        //Checking adjacent tiles for friendly units

        int newDefensiveStrength = game.getUnitAt(unitPos).getDefensiveStrength() + additionalDefensiveStrength;
        newDefensiveStrength = newDefensiveStrength * defenceMultiplier * randNumbers[1];

        System.out.println(game.getUnitAt(unitPos).getTypeString() + " defends: " + "standDefence: " + game.getUnitAt(unitPos).getDefensiveStrength() + "  newDefence: " + newDefensiveStrength + "   multiplier: "+defenceMultiplier + "    additionalDef: " + additionalDefensiveStrength + "  rand: " + randNumbers[1]);

        return newDefensiveStrength;

    }
    public int getMultiplier(Game game, Position p) {
        City city = game.getCityAt(p);
        if (city != null) {
            return 3;
        }

        if (game.getTileAt(p).getTypeString().equals(GameConstants.HILLS) || game.getTileAt(p).getTypeString().equals(GameConstants.FOREST)) {
            return 2;
        }

        return 1;
    }
    public int getNrOfNearbyFriendlyUnits(Game game, Position p) {
        Player owner = game.getUnitAt(p).getOwner();
        int additionalAttackStrength = 0;
        int i = 0;


        ArrayList<Position>list = new GameImpl(new AlphaCivFactory()).getSurroundingPositions(p);
        for (Position pos :list ) {
            if(i == 0){ i++;continue; }
            Unit friendlyUnit = game.getUnitAt(pos);
            if (friendlyUnit != null) {
                if (friendlyUnit.getOwner() == owner) {
                    additionalAttackStrength++;
                }
            }
        }
        return additionalAttackStrength;
    }
}
