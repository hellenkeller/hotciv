package hotciv.Strategies.AttackingStrategy;

import hotciv.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.Position;

public class AlphaCivAttackStrategy implements AttackingStrategy {
    @Override
    public boolean attack(Game game, Position from, Position to) {
        ((GameImpl)game).removeUnit(to);
        return true;
    }
}
