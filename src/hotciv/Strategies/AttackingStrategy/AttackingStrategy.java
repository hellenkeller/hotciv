package hotciv.Strategies.AttackingStrategy;

import hotciv.framework.Game;
import hotciv.framework.Position;

public interface AttackingStrategy {
    /**
     * This method will handle unit attacks. The method will be called when a unit is moving to a tile where an enemy unit is located
     * @param from is the position from where the attacking unit is moving
     * @param to is the position to where the attacking unit is moving
     * @return the method return a boolean, that will tell true if the attacking unit has won, and false if the attacking unit has lost, resulting in it being removed from the map.
     */
    public boolean attack(Game game, Position from, Position to);
}
