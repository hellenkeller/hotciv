package hotciv;

import hotciv.framework.City;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

public class UnitImpl implements Unit {

    private String type;    //Returning the type of unit. E.g ARCHER, SETTLER, LEGION
    private Player owner;   //Returns the owner of the unit e.g. Player.RED or BLUE
    private int moveCount;  //Defining how many moves the given unit have left, by default it is one
    private int defaultMoveCount;
    private City cityDefended;  //The city the unit is currently defending
    private int price = 0;
    private UnitElement unitElement;

    private int defensiveStrength;
    private int attackingStrength;

    UnitImpl(String type, Player owner) {
        this.type = type;
        this.owner = owner;

        initiateAttackingStrength();
        initiateDefensiveStrength();
        initiateMoveCount();
        initiatePrices();
        setUnitElement();
    }

    @Override   //Returning the type e.g ARCHER, SETTLER
    public String getTypeString() {
        return type;
    }

    @Override   //Returning the owner
    public Player getOwner() {
        return owner;
    }

    @Override   //Returning how many moves the unit have left
    public int getMoveCount() {
        return moveCount;
    }

    @Override   //Sets and gets the defensive strength
    public int getDefensiveStrength() {
        return defensiveStrength;
    }
    public void setDefensiveStrength(int i) {
        defensiveStrength = i;
    }

    @Override   //Sets and gets the attacking strength
    public int getAttackingStrength() {
        return attackingStrength;
    }
    void setAttackingStrength(int i) {
        attackingStrength = i;
    }

    //Sets and gets the defended city
    City getCityDefended() {
        return cityDefended;
    }
    void setDefendedCity(City c) {
        cityDefended = c;
    }

    //Returns the unit price.
    int getPrice() {
        return price;
    }

    //Sets and gets the unit element. Eg. Land, Water and Air
    UnitElement getUnitElement() {
        return unitElement;
    }
    private void setUnitElement() {
        if (type.equals(GameConstants.GALLEY)) {
            unitElement = UnitElement.WATER;
        } else {
            unitElement = UnitElement.LAND;
        }
    }

    //Is called when a round has ended, and resets the movecount so the unit is able to
    void resetMoveCount() {
        setMoveCount(defaultMoveCount);
    }
    //Sets the moveCount to a given value
    public void setMoveCount(int i) {
        moveCount = i;
    }

    //Initiated the diffrent values, such as attacking strength, defensive strength, price and so on. These values
    private void initiateDefensiveStrength() {
        //Setting defensive strength
        switch (getTypeString()) {
            case GameConstants.ARCHER:
                defensiveStrength = 3;
                break;
            case GameConstants.LEGION:
                defensiveStrength = 2;
                break;
            case GameConstants.SETTLER:
                defensiveStrength = 3;
                break;
            case GameConstants.GALLEY:
                defensiveStrength = 2;
                break;
        }
    }
    private void initiateAttackingStrength() {
        //Setting attacking strength
        switch (getTypeString()) {
            case GameConstants.ARCHER:
                attackingStrength = 2;
                break;
            case GameConstants.LEGION:
                attackingStrength = 4;
                break;
            case GameConstants.SETTLER:
                attackingStrength = 0;
                break;
            case GameConstants.GALLEY:
                attackingStrength = 8;
                break;
        }
    }
    private void initiatePrices() {
        switch (getTypeString()) {
            case GameConstants.ARCHER:
                price = 10;
                break;
            case GameConstants.LEGION:
                price = 15;
                break;
            case GameConstants.SETTLER:
                price = 30;
                break;
            case GameConstants.GALLEY:
                price = 30;
                break;
        }
    }
    private void initiateMoveCount() {
        if (getTypeString().equals(GameConstants.GALLEY)) {
            defaultMoveCount = 2;
        } else {
            defaultMoveCount = 1;
        }

        moveCount = defaultMoveCount;
    }



}
