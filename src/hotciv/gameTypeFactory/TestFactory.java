package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public class TestFactory implements GameTypeFactory{
    @Override
    public AgeingStrategy createAgeingStrategy() {
        return null;
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return null;
    }

    @Override
    public WinnerStrategy createWinnerStrategy(Game game) {
        return null;
    }

    @Override
    public WorldBuildingStrategy createWorldBuildingStrategy() {
        return null;
    }

    @Override
    public AttackingStrategy createAttackingStrategy(Game game) {
        return null;
    }
}
