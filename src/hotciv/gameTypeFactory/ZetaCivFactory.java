package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AgeingStrategy.AlphaCivAgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AlphaCivAttackStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.UnitActionStrategy.AlphaCivUnitActionStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WinnerStrategy.ZetaCivWinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.AlphaCivWorldBuildingStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public class ZetaCivFactory implements GameTypeFactory {
    @Override
    public AgeingStrategy createAgeingStrategy() {
        return new AlphaCivAgeingStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    @Override
    public WinnerStrategy createWinnerStrategy(Game game) {
        return new ZetaCivWinnerStrategy();
    }

    @Override
    public WorldBuildingStrategy createWorldBuildingStrategy() {
        return new AlphaCivWorldBuildingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy(Game game) {
        return new AlphaCivAttackStrategy();
    }
}
