package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AgeingStrategy.AlphaCivAgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.AttackingStrategy.EpsilonCivAttackStrategy;
import hotciv.Strategies.RandomNumberStrategy.RandomNumbers;
import hotciv.Strategies.UnitActionStrategy.AlphaCivUnitActionStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.EpsilonCivWinnerStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.AlphaCivWorldBuildingStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public class EpsilonCivFactory implements GameTypeFactory {
    @Override
    public AgeingStrategy createAgeingStrategy() {
        return new AlphaCivAgeingStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    @Override
    public WinnerStrategy createWinnerStrategy(Game game) {
        return new EpsilonCivWinnerStrategy(game);
    }

    @Override
    public WorldBuildingStrategy createWorldBuildingStrategy() {
        return new AlphaCivWorldBuildingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy(Game game) {
        return new EpsilonCivAttackStrategy(new RandomNumbers());
    }

}
