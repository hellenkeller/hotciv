package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AgeingStrategy.BetaCivAgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.AttackingStrategy.EpsilonCivAttackStrategy;
import hotciv.Strategies.RandomNumberStrategy.RandomNumbers;
import hotciv.Strategies.UnitActionStrategy.SemiCivUnitActionStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.EpsilonCivWinnerStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.DeltaCivWorldBuildingStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public class SemiCivFactory implements GameTypeFactory {
    @Override
    public AgeingStrategy createAgeingStrategy() {
        return new BetaCivAgeingStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new SemiCivUnitActionStrategy();
    }

    @Override
    public WinnerStrategy createWinnerStrategy(Game game) {
        return new EpsilonCivWinnerStrategy(game);
    }

    @Override
    public WorldBuildingStrategy createWorldBuildingStrategy() {
        return new DeltaCivWorldBuildingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy(Game game) {
        return new EpsilonCivAttackStrategy(new RandomNumbers());
    }
}
