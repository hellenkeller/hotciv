package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public interface GameTypeFactory {
    /**
     * @return all the methods return the requested strategy. eg. AlphaCivFactory will return AlphaCivWinnerStrategy, AlphaCivUnitStrategy and so on.
     */

    public AgeingStrategy createAgeingStrategy();
    public UnitActionStrategy createUnitActionStrategy();
    public WinnerStrategy createWinnerStrategy(Game game);
    public WorldBuildingStrategy createWorldBuildingStrategy();
    public AttackingStrategy createAttackingStrategy(Game game);
}
