package hotciv.gameTypeFactory;

import hotciv.Strategies.AgeingStrategy.AgeingStrategy;
import hotciv.Strategies.AgeingStrategy.AlphaCivAgeingStrategy;
import hotciv.Strategies.AttackingStrategy.AlphaCivAttackStrategy;
import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.UnitActionStrategy.GammaCivUnitActionStrategy;
import hotciv.Strategies.UnitActionStrategy.UnitActionStrategy;
import hotciv.Strategies.WinnerStrategy.AlphaCivWinnerStrategy;
import hotciv.Strategies.WinnerStrategy.WinnerStrategy;
import hotciv.Strategies.WorldBuildingStrategy.AlphaCivWorldBuildingStrategy;
import hotciv.Strategies.WorldBuildingStrategy.WorldBuildingStrategy;
import hotciv.framework.Game;

public class GammaCivFactory implements GameTypeFactory {
    @Override
    public AgeingStrategy createAgeingStrategy() {
        return new AlphaCivAgeingStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new GammaCivUnitActionStrategy();
    }

    @Override
    public WinnerStrategy createWinnerStrategy(Game game) {
        return new AlphaCivWinnerStrategy();
    }

    @Override
    public WorldBuildingStrategy createWorldBuildingStrategy() {
        return new AlphaCivWorldBuildingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy(Game game) {
        return new AlphaCivAttackStrategy();
    }
}
