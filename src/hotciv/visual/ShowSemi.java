package hotciv.visual;

import hotciv.GameImpl;
import hotciv.Tools.CompositionTool;
import hotciv.framework.Game;
import hotciv.gameTypeFactory.SemiCivFactory;
import hotciv.view.GfxConstants;
import hotciv.view.TextFigure;
import minidraw.framework.DrawingEditor;
import minidraw.standard.MiniDrawApplication;

import java.awt.*;

public class ShowSemi {

    public static void main(String[] args) {

        Game game = new GameImpl(new SemiCivFactory());

        DrawingEditor editor = new MiniDrawApplication("SemiCiv Baby!!",new HotCivFactory4(game));

        editor.open();

        TextFigure tf = new TextFigure("4000 BC", new Point(GfxConstants.AGE_TEXT_X, GfxConstants.AGE_TEXT_Y) );

        //editor.drawing().add(tf);

        editor.setTool(new CompositionTool(game, editor));
    }

}
