package hotciv;

import hotciv.framework.*;
import hotciv.gameTypeFactory.AlphaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Skeleton class for AlphaCiv test cases
 * <p>
 * Updated Oct 2015 for using Hamcrest matchers
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TestAlphaCiv {
    private Game game;
    private Position redCity = new Position(1,1);
    private Position blueCity = new Position(4,1);

    /**
     * Fixture for alphaciv testing.
     */
    @Before
    public void setUp() {
        game = new GameImpl(new AlphaCivFactory());
    }

    // FRS p. 455 states that 'Red is the first player to take a turn'.
    @Test //Testing that red is the starting player
    public void shouldBeRedAsStartingPlayer() {
        assertNotNull(game);
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test //Testing, that blue is the player in turn after red
    public void blueShouldBeThePlayerInTurnAfterFirstEndOfTurn() {
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
    }

    @Test //Again, after blue have had his turn, it red should have a turn again.
    public void afterTwoTurnsItShouldBeREDTurn() {
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test //Testing that the game starts at age
    public void gameShouldStartAt4000bc() {


        assertThat(game.getAge(), is(-4000));
    }

    @Test //Testing that the game age advances with 100 years each round (not to be confused with each turn)
    public void eachRoundAdvancesAgeWith100Years() {
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getAge(), is(-4000 + 100));
    }

    @Test //This test is surely to be deleted later on.
    public void thereShouldBeNoWinnerBefore3000BC() {
        while (game.getAge() != -3000) {
            assertThat(game.getWinner(), is(nullValue()));
            game.endOfTurn();
        }
    }

    @Test //Testing that both our cities at owned by a player (A city can not be created without a Player as a owner)
    public void citiesAreOwnedByOneOfThePlayers() {
        assertThat(game.getCityAt(new Position(1, 1)).getOwner(), is(notNullValue()));
        assertThat(game.getCityAt(new Position(4, 1)).getOwner(), is(notNullValue()));
    }

    @Test //Testing that red city is at position (1,1)
    public void redCityShouldBeAt11() {
        assertThat(game.getCityAt(redCity).getOwner(), is(Player.RED));
    }

    @Test //Testing that blue city is at position (4,1)
    public void blueCityShouldBeAt41() {
        Position cityPos = new Position(4, 1);
        assertThat(game.getCityAt(cityPos).getOwner(), is(Player.BLUE));
    }

    @Test //tester nogen random felter om de er plain
    public void allTilesShouldBePlainExceptSome() {
        Position randomPos1 = new Position(15, 15);
        Position randomPos2 = new Position(3, 7);

        assertThat(game.getTileAt(randomPos1).getTypeString(), is(GameConstants.PLAINS));
        assertThat(game.getTileAt(randomPos2).getTypeString(), is(GameConstants.PLAINS));
    }

    @Test //Testing that tile (0,1) is not type PLAINS, since it should be HILLS
    public void tile01ShouldBeHills() {
        Position position = new Position(0, 1);
        assertThat(game.getTileAt(position).getTypeString(), is(GameConstants.HILLS));
    }

    @Test //Testing that the red archer is placed at position (2,0)
    public void redShouldHaveAArcherAt20() {
        Position position = new Position(2, 0);
        assertThat(game.getUnitAt(position).getOwner(), is(notNullValue()));
    }

    @Test //Testing the blue have a legion unit at position (3,2)
    public void blueShouldHaveALegionAt32() {
        Position position = new Position(3, 2);
        assertThat(game.getUnitAt(position).getOwner(), is(Player.BLUE));
        assertThat(game.getUnitAt(position).getTypeString(), is(GameConstants.LEGION));
    }

    @Test //Testing that red have a settler unit at (4,3)
    public void redShouldHaveASettlerAt43() {
        Position position = new Position(4, 3);
        assertThat(game.getUnitAt(position).getOwner(), is(Player.RED));
        assertThat(game.getUnitAt(position).getTypeString(), is(GameConstants.SETTLER));
    }

    @Test //Testing that units can't move on top of each other
    public void oneUnitCannotMoveOnTopOfAnotherUnit() {
        Position postFrom = new Position(3, 2);
        Position postTo = new Position(4, 3);
        assertThat(game.moveUnit(postFrom, postTo), is(false));
    }

    @Test //Testing that units can move to empty tiles, (that is ofc given that the tile supports movement)
    public void unitCanMoveToAnEmptyTile() {
        game.endOfTurn();   //Ending Turn so it will be blue player, so we can move the blue unit
        Position posFrom = new Position(3, 2);
        Position posTo = new Position(3, 1);
        assertThat(game.moveUnit(posFrom, posTo), is(true));
    }

    @Test
    //Checking if the move method actually works, by testing that the unit is at the tile we requested it to move on
    public void checkIfUnitMoved() {
        game.endOfTurn();   //End turn so it will be blue's turn, since we are testing with a blue unit
        Position posFrom = new Position(3, 2);
        Position posTo = new Position(3, 1);
        game.moveUnit(posFrom, posTo);
        Position newPosition = new Position(3, 1);
        assertThat(game.getUnitAt(newPosition).getTypeString(), is(GameConstants.LEGION));
    }

    @Test //Testing if the unit is able to move outside the world (it shouldn't be)
    public void unitsShouldNotBeAbleToMoveOutsideTheWorld() {
        Position posFrom = new Position(2, 0);
        Position posTo = new Position(2, -1);
        assertThat(game.moveUnit(posFrom, posTo), is(false));
    }

    @Test //A unit should not be able to move on mountains, therefor the move method should return false
    public void unitShouldNotBeAbleToMoveOnMountain() {
        Position posFrom = new Position(3, 2);
        Position posTo = new Position(2, 2);
        assertThat(game.moveUnit(posFrom, posTo), is(false));
    }

    @Test //A unit should not be able to move on oceans, therefor the move method should return false
    public void unitShouldNotBeAbleToMoveOnOcean() {
        Position posFrom = new Position(2, 0);
        Position posTo = new Position(1, 0);
        assertThat(game.moveUnit(posFrom, posTo), is(false));
    }

    @Test //red should not be able to control blue units, and vise versa blue should not be able to control red units
    public void redShouldNotBeAbleToControlBlueUnits() {
        Position posFrom = new Position(3, 2);
        Position posTo = new Position(4, 2);
        assertThat(game.moveUnit(posFrom, posTo), is(false));
    }

    @Test //Testing the defensive strengh of an archer
    public void archerDefenseiveStrengthShouldBe3() {
        Unit archer = game.getUnitAt(new Position(2, 0));    //We know that the archer is at the given position
        assertThat(archer.getDefensiveStrength(), is(3));
    }

    @Test //Testing the attacking strength of legion
    public void legionAttackingStrengthShouldBe4() {
        Unit legion = game.getUnitAt(new Position(3, 2));    //We know that the legion is at the given position
        assertThat(legion.getAttackingStrength(), is(4));
    }

    @Test //is the position of a unit is the same as the city, the unit is defending the city
    public void aUnitThatIsLocatedAtACityTileIsDefending() {

        Position posFrom = new Position(3, 2);
        Position posTo = new Position(4, 1);
        game.endOfTurn();
        assertThat(((UnitImpl) game.getUnitAt(posFrom)).getCityDefended(), is(nullValue()));
        game.moveUnit(posFrom, posTo);
        assertThat(((UnitImpl) game.getUnitAt(posTo)).getCityDefended(), is(game.getCityAt(posTo)));
    }

    @Test //if a defending unit moves away, it stops defending the city
    public void ifAUnitMovesFromTheCityStopsDefending() {
        Position posFrom = new Position(3, 2);
        Position posTo = new Position(4, 1);
        game.endOfTurn();
        game.moveUnit(posFrom, posTo);

        game.endOfTurn();
        game.endOfTurn();

        Position posFrom1 = new Position(4, 1);
        Position posTo1 = new Position(4, 2);
        game.moveUnit(posFrom1, posTo1);
        assertThat(((UnitImpl) game.getUnitAt(posTo1)).getCityDefended(), is(nullValue()));


    }

    @Test
    //if city is not owned by the attacking unit, the city will change ownership, and the attacking unit will be set as defending
    public void changeOfCityOwnerAfterAttack() {
        game.moveUnit(new Position(2, 0), new Position(3, 1));

        game.endOfTurn();   //Ending turn so we can move the unit again
        game.endOfTurn();

        game.moveUnit(new Position(3, 1), new Position(4, 1));

        assertThat(game.getCityAt(new Position(4, 1)).getOwner(), is(Player.RED));
    }

    @Test //A unit can only move once every turn
    public void unitShouldOnlyBeAbleToMoveOncePrTurn() {
        game.moveUnit(new Position(2, 0), new Position(3, 0));
        assertThat(game.moveUnit(new Position(3, 0), new Position(4, 0)), is(false));
    }

    @Test //Testing our distance algorithm by testing if units can move more than one distance per round.
    public void movementsAreMeasured() {

        assertThat(game.moveUnit(new Position(2, 0), new Position(4, 0)), is(false));
    }

    @Test //A unit can not move longer than one distance vertical, horizontal or diagonal
    public void unitsShouldOnlyMoveOneDistance() {
        assertThat(game.moveUnit(new Position(2, 0), new Position(4, 0)), is(false));
    }

    @Test //Units do not "store" moving capabilities. It will always only be able to move one length every turn
    public void unitsCannotMoveTwiceAsLongAfterResting() {
        assertThat(game.getUnitAt(new Position(2, 0)).getMoveCount(), is(1));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(2, 0), new Position(4, 0)), is(false));
    }

    @Test //Every turn, cities get six productions
    public void cityProduceSixProductions() {

        assertThat(((CityImpl) game.getCityAt(new Position(1, 1))).getAccumulatedProduction(), is(0));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(((CityImpl) game.getCityAt(new Position(1, 1))).getAccumulatedProduction(), is(0 + 6));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(((CityImpl) game.getCityAt(new Position(1, 1))).getAccumulatedProduction(), is(6 + 6));
    }

    @Test //tile type PLAINS produce 3 food
    public void plainsProduceThreeFood() {
        assertThat(((TileImpl) game.getTileAt(new Position(5, 1))).getProductionAmount(), is(3));
        assertThat(((TileImpl) game.getTileAt(new Position(5, 1))).getProductionType(), is("Food"));
    }

    @Test //tile type OCEANS produce 1 food
    public void oceansProduceOneFood() {
        assertThat(((TileImpl) game.getTileAt(new Position(1, 0))).getProductionAmount(), is(1));
        assertThat(((TileImpl) game.getTileAt(new Position(1, 0))).getProductionType(), is("Food"));
    }

    @Test //tile type MOUNTAINS produce 1 productions
    public void mountainProduceOneProduction() {
        assertThat(((TileImpl) game.getTileAt(new Position(2, 2))).getProductionAmount(), is(1));
        assertThat(((TileImpl) game.getTileAt(new Position(2, 2))).getProductionType(), is("Production"));
    }

    @Test //tile type FORREST produce 3 productions
    public void forestProduceThreeProductions() {
        assertThat(new TileImpl(GameConstants.FOREST).getProductionAmount(), is(3));
        assertThat(new TileImpl(GameConstants.FOREST).getProductionType(), is("Production"));
    }

    @Test //tile type HILLS produce 2 productions
    public void hillsProduceTwoProductions() {
        assertThat(((TileImpl) game.getTileAt(new Position(0, 1))).getProductionAmount(), is(2));
        assertThat(((TileImpl) game.getTileAt(new Position(0, 1))).getProductionType(), is("Production"));
    }

    @Test //If redCity's production is set to archer, it's production should be archer
    public void redCityProductionShouldBeArcher() {
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        assertThat(game.getCityAt(new Position(1, 1)).getProduction(), is(GameConstants.ARCHER));
    }

    @Test
    //If redCity's production is set to archer, an archer should be produced after one round, since the city then would have accumulated 12 production, and an archer only cost 10 production
    public void redCityShouldProduceArcherWhenAvailable() {
        //Making sure that there is no units at 1,1 before
        assertThat(game.getUnitAt(redCity), is(nullValue()));
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);

        //adding 4 production
        ((CityImpl) game.getCityAt(redCity)).addProduction(4);
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(new Position(1, 1)).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    //If redCity's production is set to legion, an legion should be produced after two rounds, since the city then would have accumulated 18 production, and an legion only cost 15 production
    public void redCityShouldProduceLegionWhenAvailable() {
        Position posOfCity = new Position(1, 1);
        assertThat(game.getUnitAt(posOfCity), is(nullValue()));

        game.changeProductionInCityAt(new Position(1, 1), GameConstants.LEGION);

        //Adding 9 production, since game.endRound() will add 6 production by itself. 9+6=15 <- enought to buy a legion
        ((CityImpl) game.getCityAt(posOfCity)).addProduction(9);

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(posOfCity).getTypeString(), is(GameConstants.LEGION));
    }

    @Test
    //If redCity's production is set to settler, an settler should be produced after four rounds, since the city then would have accumulated 30 production, and an settler cost 30 production
    public void redCityShouldProduceSettlerWhenAvailable() {
        Position posOfCity = new Position(1, 1);
        assertThat(game.getUnitAt(posOfCity), is(nullValue()));

        game.changeProductionInCityAt(new Position(1, 1), GameConstants.SETTLER);
        ((CityImpl) game.getCityAt(posOfCity)).addProduction(26);

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getUnitAt(posOfCity).getTypeString(), is(GameConstants.SETTLER));
    }

    @Test //After one round, the city will have 12 production, and then use 10 on buying an archer.
    public void whenBuyingArcherRedCityShouldLooseSixProduction() {
        City city = game.getCityAt(new Position(1, 1));
        ((CityImpl) city).setProduction(GameConstants.ARCHER);

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        assertThat(((CityImpl) city).getAccumulatedProduction(), is(6 + 6 - 10));
    }

    @Test //After two rounds, the city will have 18 production, and then use 15 on buying an legion
    public void whenBuyingLegionRedCityShouldLooseSixProduction() {
        City city = game.getCityAt(new Position(1, 1));
        ((CityImpl) city).setProduction(GameConstants.LEGION);

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        assertThat(((CityImpl) city).getAccumulatedProduction(), is(6 + 6 + 6 - 15));
    }

    @Test //After four rounds, the city will have 30 production, and then use 30 on buying an settler
    public void whenBuyingSettlerRedCityShouldLoose30Production() {
        City city = game.getCityAt(new Position(1, 1));
        ((CityImpl) city).setProduction(GameConstants.SETTLER);

        //Running endOfTurn() to get the required amount of production
        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        game.endOfTurn();
        game.endOfTurn();

        assertThat(((CityImpl) city).getAccumulatedProduction(), is(6 + 6 + 6 + 6 + 6 - 30));
    }

    @Test //If workForceBalance is set to food, then the workForceBalance should be food.
    public void workForceBalanceShouldBeFoodWhenSetToFood() {
        game.changeWorkForceFocusInCityAt(new Position(1, 1), GameConstants.foodFocus);

        assertThat(game.getCityAt(new Position(1, 1)).getWorkforceFocus(), is(GameConstants.foodFocus));
    }

    @Test //If workForceBalance is set to production, then the workForceBalance should be production.
    public void workForceBalanceShouldBeProductionWhenSetToProduction() {
        game.changeWorkForceFocusInCityAt(new Position(1, 1), GameConstants.productionFocus);

        assertThat(game.getCityAt(new Position(1, 1)).getWorkforceFocus(), is(GameConstants.productionFocus));
    }


    /**
     * Spawn function should spawn units on the
     * city, and then clockwise around a city,
     * if the city tile is occupied.
     * <p>
     * Below there will be 7 test methods for checking the spawn mechanism.
     */
    @Test
    public void firstUnitShouldSpawnOnTheCity() {
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        ((CityImpl) game.getCityAt(redCity)).addProduction(10);   //Making sure the city have enough production for archers

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(this.redCity).getTypeString(),is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(0,1)),is(nullValue()));
    }
    @Test
    public void secondUnitShouldSpawnNorthOfTheCity(){
        ((CityImpl) game.getCityAt(redCity)).addProduction(20);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(new Position(0,1)).getTypeString(),is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(0,2)),is(nullValue()));
    }
    @Test
    public void thirdUnitShouldSpawnNorthEastOfTheCity() {
        ((CityImpl) game.getCityAt(redCity)).addProduction(30);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(new Position(0, 2)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(1, 2)), is(nullValue()));
    }
    @Test
    public void forthUnitShouldSpawnEastOfTheCity(){
        ((CityImpl) game.getCityAt(redCity)).addProduction(40);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        for (int i = 0; i < 3; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(new Position(1, 2)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(2, 2)), is(nullValue()));
    }
    @Test   //The is a mountain in the way, so it should choose the next available position
    public void fifthUnitShouldSpawnSouthOfTheCity(){
        ((CityImpl) game.getCityAt(redCity)).addProduction(50);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        for (int i = 0; i < 4; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(new Position(2, 1)).getTypeString(), is(GameConstants.ARCHER));
    }
    @Test
    public void sixthUnitShouldSpawnNorthWestOfTheCity(){
        ((CityImpl) game.getCityAt(redCity)).addProduction(60);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        for (int i = 0; i < 5; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(true));
        assertThat(game.getUnitAt(new Position(0, 0)).getTypeString(), is(GameConstants.ARCHER));
    }
    @Test
    public void seventhUnitShouldNotSpawn(){
        ((CityImpl) game.getCityAt(redCity)).addProduction(70);
        game.changeProductionInCityAt(redCity, GameConstants.ARCHER);
        for (int i = 0; i < 6; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(((GameImpl) game).produceUnit(redCity, Player.RED, GameConstants.ARCHER), is(false));

    }
}
