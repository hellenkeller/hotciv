package hotciv;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
    @Suite.SuiteClasses({
            TestAlphaCiv.class,
            TestBetaCiv.class,
            TestGammaCiv.class,
            TestDeltaCiv.class,
            TestEpsilonCiv.class,
            TestZetaCiv.class,
            TestThetaCiv.class,
            TestSemiCiv.class,
            TestIntergration.class,
            TestGameObserver.class,
            TestTranscriptTranscriptDecorator.class,
    })

public class TestAll {
    //Don't really need this tbh.
}
