package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameObserver;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.AlphaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestGameObserver {

    Game game;

    private Position worldChangesPos;
    private Player playerNextTurn;
    private int gameAge;

    @Before
    public void setUp(){
        game = new GameImpl(new AlphaCivFactory());

        GameObserver gameObserver = new GameObserver() {
            @Override
            public void worldChangedAt(Position pos) {
                worldChangesPos = pos;
            }

            @Override
            public void turnEnds(Player nextPlayer, int age) {
                playerNextTurn = nextPlayer;
                gameAge = age;
            }

            @Override
            public void tileFocusChangedAt(Position position) {
            }
        };

        game.addObserver(gameObserver);

    }

    @Test
    public void endTurnNotifiesCorrectly(){
        game.endOfTurn();

        assertThat(playerNextTurn,is(Player.BLUE));
        assertThat(gameAge,is(-4000));

    }

    @Test
    public void worldChangeNotifiesCorrectly(){
        //When moving archer, the worldChangePos should be the position he is moving to

        assertThat(game.moveUnit(new Position(2,0),new Position(3,0)),is(true));
        assertThat(worldChangesPos,is(new Position(3,0)));
    }

}
