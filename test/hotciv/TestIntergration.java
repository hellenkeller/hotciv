package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.*;
import org.junit.Test;
import testStumps.FixedRandomNumbers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class TestIntergration {
    private Game game;

    /**
     * Fixture for alphaciv testing.
     */
    @Test
    public void alphaCivWinnerImplementsCorrectly() {
        //At 3000BC, red should be the winner
        game = new GameImpl(new AlphaCivFactory());
        while (game.getAge() != -3000) {
            game.endOfTurn();
            game.endOfTurn();
        }
        assertTrue(game.getWinner() == Player.RED);
    }

    @Test
    public void betaCivWinnerImplementsCorrectly() {
        game = new GameImpl(new BetaCivFactory());

        game.endOfTurn();
        //Setting the same owner in the two cities. This should give BLUE as winner
        ((CityImpl) game.getCityAt(new Position(1,1))).setCityOwner(Player.BLUE);
        assertThat(game.getWinner(), is(Player.BLUE));
        game.endOfTurn();
        //Setting red as owner of one of the city's, which shouldn't give any winner
        ((CityImpl) game.getCityAt(new Position(4, 1))).setCityOwner(Player.RED);
        assertThat(game.getWinner(), is(nullValue()));


    }

    @Test   //AlphaCiv will always advance time with 100 years.
    public void alphaCivAgeingImplementsCorrectly() {
        game = new GameImpl(new AlphaCivFactory());

        //Since each round advances with 100 years. After 40 rounds, it will have advanced by 4000 years, and the year should therefore be 0
        for (int i = 0; i < 40; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(0));

    }

    @Test   //BetaCiv ages following a set of rules
    public void betaCivAgeingImplementsCorrectly() {
        game = new GameImpl(new BetaCivFactory());

        //Following the rules set, after 39 rounds, the year would be -100, just as with the alphaCiv
        for (int i = 0; i < 39; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-100));

        //This is where the betaCiv is diffrent from alphaCiv, because after this round the year should be -1, and not 0 as with alphaCiv));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(), is(-1));
    }

    @Test
    //Testing that gammaCiv implements correctly. We do this by checking that the settler is replaced by a city when it performs it's unit action
    public void gammaCivUnitActionImplementsCorrectly() {
        game = new GameImpl(new GammaCivFactory());

        Position unitPos = new Position(4, 3);

        game.performUnitActionAt(unitPos);

        assertThat(game.getUnitAt(unitPos), is(nullValue()));
        assertThat(game.getCityAt(unitPos).getOwner(), is(Player.RED));

    }

    @Test   //Testing that the alphaCivWorldBuildStrategy implements correctly
    public void alphaCivWorldBuilderImplementsCorrectly() {
        game = new GameImpl(new AlphaCivFactory());

        assertThat(game.getUnitAt(new Position(2, 0)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(3, 8)), is(nullValue()));
    }

    @Test   //Testing that the deltaCivWorldBuilderStrategy implements correctly
    public void deltaCivWorldBuilderImplementsCorrectly() {
        game = new GameImpl(new DeltaCivFactory());

        assertThat(game.getUnitAt(new Position(2, 0)), is(nullValue()));
        assertThat(game.getUnitAt(new Position(3, 8)).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void alphaCivAttackImplementsCorrectly() {
        game = new GameImpl(new AlphaCivFactory());
        Position posFrom = new Position(4, 3);
        Position posTo = new Position(3, 2);

        assertThat(game.moveUnit(posFrom, posTo), is(true));

        assertThat(game.getUnitAt(posTo).getTypeString(), is(GameConstants.SETTLER));
        assertThat(game.getUnitAt(posFrom), is(nullValue()));

    }

    @Test
    public void fixedRandomNumbersImplementsCorrectly(){
        FixedRandomNumbers fixedRandomNumbers = new FixedRandomNumbers();
        int[]numbers = fixedRandomNumbers.generateNumbers();

        assertThat(numbers[0],is(1));
        assertThat(numbers[1],is(1));
    }

    @Test   //What makes epsilon diffrent, is that now you are able to loose when moving
    public void epsilonAttackStrategyImplementsCorrectly() {
        game = new GameImpl(new EpsilonCivFactory());
        //Using the testFactory, so i don't get trouble with random numbers
        ((GameImpl) game).produceUnit(new Position(1, 5), Player.RED, GameConstants.SETTLER);
        ((GameImpl) game).produceUnit(new Position(2, 5), Player.BLUE, GameConstants.ARCHER);

        assertThat(game.moveUnit(new Position(1, 5), new Position(2, 5)), is(false));
    }

    @Test
    public void epsilonWinnerStrategyImplementsCorrectly() {
        game = new GameImpl(new EpsilonCivFactory());

        ((GameImpl) game).produceUnit(new Position(2, 5), Player.RED, GameConstants.LEGION);

        //Setting the attacking strength to 100, to make sure red wins
        ((UnitImpl) game.getUnitAt(new Position(2, 5))).setAttackingStrength(100);


        ((GameImpl) game).produceUnit(new Position(2, 6), Player.BLUE, GameConstants.LEGION);
        ((GameImpl) game).produceUnit(new Position(2, 7), Player.BLUE, GameConstants.LEGION);
        ((GameImpl) game).produceUnit(new Position(2, 8), Player.BLUE, GameConstants.LEGION);

        assertThat(game.moveUnit(new Position(2, 5), new Position(2, 6)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.moveUnit(new Position(2, 6), new Position(2, 7)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.moveUnit(new Position(2, 7), new Position(2, 8)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void zetaCivWinnerStrategyImplementsCorrectly() {
        game = new GameImpl(new ZetaCivFactory());

        //There is till no winner after 10 rounds
        for (int i = 0; i < 11; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(game.getWinner(), is(nullValue()));
    }

    @Test
    public void randomWorldGeneratorImplementsCorrectly(){
        /*game = new GameImpl(new AlphaCivFactory());
        WorldBuildingStrategy worldBuildingStrategy = new FractalWorldGenerator();

        worldBuildingStrategy.createWorld(game);*/

    }
}
