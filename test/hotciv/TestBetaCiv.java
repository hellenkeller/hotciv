package hotciv;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.BetaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestBetaCiv {
    private Game game;

    /** Fixture for alphaciv testing. */
    @Before
    public void setUp() {
        game = new GameImpl(new BetaCivFactory());
    }

    @Test   //Red should be the winner, if we set blue city to be owned by red
    public void redShouldBeTheWinner(){
        ((CityImpl)game.getCityAt(new Position(4,1))).setCityOwner(Player.RED);
        assertThat(game.getWinner(),is(Player.RED));
    }

    @Test   //blue should be the winner, if we set red city to be owned by blue
    public void blueShouldBeTheWinner(){
        game.endOfTurn();
        ((CityImpl)game.getCityAt(new Position(1,1))).setCityOwner(Player.BLUE);
        assertThat(game.getWinner(),is(Player.BLUE));
    }


    /**
     * //BetaCiv should age as described in 36.3.1
     */
    @Test
    public void betaCivShouldAge100Between4000BCAnd100BC() {

        for(int i = 0; i<39;i++){
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(game.getAge(),is(-100));
    }

    @Test   //BetaCiv should follow the sequence given in the book 100BC, 1BC, 1AD and 50AD
    public void betaCivShouldFollowTheSequenceAroundChrist(){
        for(int i = 0; i< 39;i++){  //Running the necessary number of rounds to advance to 100BC
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(),is(-100));

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(-1));

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1));

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(50));
    }

    @Test
    public void betaCivShouldAge50Between50ADAnd1750AD(){
        for(int i = 0; i<42;i++){   //Running the necessary number of rounds to advance to 50AD
            game.endOfTurn();
            game.endOfTurn();
        }

        assertThat(game.getAge(),is(50));

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(50+50));

        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(50+50+50));
    }

    @Test
    public void betaCivShouldAge25Between1750And1900(){
        for(int i = 0; i<76;i++){   //Running the necessary number of rounds to advance to 1750AD
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(),is(1750));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1750+25));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1750+25+25));   //<--Evident data (note)
    }

    @Test
    public void betaCivShouldAge5Between1900And1970(){
        for(int i = 0; i<82;i++){   //Running the necessary number of rounds to advance to 1900AD
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(),is(1900));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1900+5));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1900+5+5));
    }

    @Test
    public void betaCivShouldAge1After1970(){
        for(int i = 0; i<96;i++){   //Running the necessary number of rounds to advance to 1970AD
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getAge(),is(1970));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1970+1));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getAge(),is(1970+1+1));
    }
}

