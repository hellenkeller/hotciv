package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.GammaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;


public class TestGammaCiv {
    private Game game;

    @Before
    public void setUp(){
        game = new GameImpl(new GammaCivFactory());
    }
    @Test
    public void twoPlusOneIsThree(){
        assertThat(2+1,is(3));
    }
    @Test
    public void settlerActionRemovesSettler(){
        Position settlerPos = new Position(4,3);

        assertThat(game.getUnitAt(settlerPos).getTypeString(),is(GameConstants.SETTLER));
        game.performUnitActionAt(settlerPos);
        assertThat(game.getUnitAt(settlerPos),is(nullValue()));
    }

    @Test
    public void settlerActionSpawnsNewCity(){
        Position settlerPosition = new Position(4,3);

        //Making sure there is no city beforehand
        assertThat(game.getCityAt(settlerPosition),is(nullValue()));
        game.performUnitActionAt(settlerPosition);
        assertThat(game.getCityAt(settlerPosition).getOwner(),is(Player.RED));
    }

    @Test
    public void whenArcherPerformsUnitActionItCannotMove(){
        assertThat(game.getUnitAt(new Position(2,0)).getMoveCount(),is(1));

        game.performUnitActionAt(new Position(2,0));
        assertThat(game.moveUnit(new Position(2,0),new Position(2,1)),is(false));
    }

    @Test
    public void archerCanDoubleItsDefensiveStrength(){
        assertThat(game.getUnitAt(new Position(2,0)).getDefensiveStrength(),is(3));
        game.performUnitActionAt(new Position(2,0));
        assertThat(game.getUnitAt(new Position(2,0)).getDefensiveStrength(),is(3*2));
    }

    @Test
    public void archerCannotMoveWhileFortified(){
        Position archerPos = new Position(2,0);
        game.performUnitActionAt(archerPos);

        assertThat(game.moveUnit(archerPos,new Position(3,0)),is(false));

        //Making sure that end of round doesn't unintentionally restore archers ability to move
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.moveUnit(archerPos,new Position(3,0)),is(false));
    }

    @Test
    public void archerMobilityRestoresWhenUsingActionAgain(){
        Position archerPos = new Position(2,0);
        game.performUnitActionAt(archerPos);

        assertThat(game.moveUnit(archerPos,new Position(3,0)),is(false));

        game.performUnitActionAt(archerPos);

        assertThat(game.moveUnit(archerPos,new Position(3,0)),is(true));
    }

    @Test
    public void archerDefenseValueRestoresAfterUsingActionAgain(){
        Position archerPos = new Position(2,0);
        game.performUnitActionAt(archerPos);

        assertThat(game.getUnitAt(archerPos).getDefensiveStrength(),is(3*2));

        game.performUnitActionAt(archerPos);

        assertThat(game.getUnitAt(archerPos).getDefensiveStrength(),is(3));
    }

    @Test //Blue and red shouldn't be able to perform unit actions on the opponents units
    public void blueCantUseRedUnitsAction(){
        game.endOfTurn();

        game.performUnitActionAt(new Position(2,0));
        assertThat(game.getUnitAt(new Position(2,0)).getDefensiveStrength(),is(3));
        assertThat(game.getUnitAt(new Position(2,0)).getMoveCount(),is(1));
    }


}

