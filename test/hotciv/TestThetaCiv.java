package hotciv;

import hotciv.Strategies.UnitActionStrategy.ThetaCivUnitActionStrategy;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.ThetaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class TestThetaCiv {

    private Game game = new GameImpl(new ThetaCivFactory());
    private Position redGalley = new Position(3,10);
    private Position blueGalley = new Position(2,10);

    @Before
    public void setUp(){
        ((GameImpl)game).produceUnit(redGalley,Player.RED,GameConstants.GALLEY);
        ((GameImpl)game).produceUnit(blueGalley,Player.BLUE,GameConstants.GALLEY);
    }
    @Test
    public void galleyHaveEightAttackStrength(){
        assertThat(new UnitImpl(GameConstants.GALLEY,Player.RED).getAttackingStrength(),is(8));
    }

    @Test
    public void galleyHaveTwoDefensiveStrength(){
        assertThat(new UnitImpl(GameConstants.GALLEY,Player.RED).getDefensiveStrength(),is(2));
    }
    @Test
    public void galleyCanMoveOnWater(){
        assertThat(game.moveUnit(redGalley,new Position(3,11)),is(true));
    }
    @Test
    public void galleyCanMoveTwice(){
        assertThat(game.moveUnit(redGalley,new Position(3,11)),is(true));
        assertThat(game.moveUnit(new Position(3,11),new Position(2,11)),is(true));
    }
    @Test
    public void galleyCanAttackOnWater(){
        ((GameImpl)game).produceUnit(new Position(3,11),Player.BLUE,GameConstants.GALLEY);
        assertThat(game.moveUnit(redGalley,new Position(3,11)),is(true));
    }
    @Test
    public void galleyCannotMoveOnLand(){
        assertThat(game.moveUnit(redGalley,new Position(4,10)),is(false));
    }
    @Test
    public void galleyCannotAttackOnLand(){
        ((GameImpl)game).produceUnit(new Position(3,9),Player.BLUE,GameConstants.SETTLER);
        assertThat(game.moveUnit(redGalley,new Position(3,9)),is(false));
    }
    @Test
    public void citiesAwayFromWaterCannotProduceGalley(){
        assertThat(((GameImpl)game).produceUnit(new Position(4,5),Player.BLUE,GameConstants.GALLEY),is(false));
    }
    @Test
    public void citiesAtWaterCanProduceGalley(){
        assertThat(((GameImpl)game).produceUnit(new Position(0,3),Player.RED,GameConstants.GALLEY),is(true));
        assertThat(game.getUnitAt(new Position(0,2)).getTypeString(),is(GameConstants.GALLEY));
    }
    @Test
    public void galleyUnitActionSpawnsASettlerOnAdjacentTiles(){
        game.performUnitActionAt(redGalley);
        assertThat(game.getUnitAt(new Position(4,11)).getTypeString(),is(GameConstants.SETTLER));
    }

    /**
     * Unit testing
     */

    @Test
    public void performUnitActionDoesNothingIfThereIsNoUnit(){
        ThetaCivUnitActionStrategy thetaCivUnitActionStrategy = new ThetaCivUnitActionStrategy();
        thetaCivUnitActionStrategy.performUnitAction(game,new Position(0,0));
        assertThat(game.getUnitAt(new Position(0,0)),is(nullValue()));
    }

    @Test
    public void performUnitActionDoesNothingIfThereIsNoLand(){
        ((GameImpl)game).produceUnit(new Position(0,15),Player.RED,GameConstants.GALLEY);
        game.performUnitActionAt(new Position(0,15));
        assertThat(game.getUnitAt(new Position(0,15)).getTypeString(),is(GameConstants.GALLEY));
    }
}
