package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.SemiCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class TestSemiCiv {
    Game game;

    @Before
    public void setUp(){
        game=new GameImpl(new SemiCivFactory());
    }

    @Test
    public void performUnitActionWorksWithSettler(){
        Position pos = new Position(0,3);
        ((GameImpl)game).produceUnit(pos,Player.RED,GameConstants.SETTLER);

        game.performUnitActionAt(pos);

        assertThat(game.getCityAt(pos),is(notNullValue()));
    }

    @Test
    public void performUnitActionDoesNotWorkWithArcher(){
        Position pos = new Position(0,3);

        ((GameImpl)game).produceUnit(pos,Player.RED,GameConstants.ARCHER);

        //Since i know archer moveCount will be set to -1 after performUnitAction, i can test it that way
        game.performUnitActionAt(pos);
        assertThat(game.getUnitAt(pos).getMoveCount(),is(1));
    }
}
