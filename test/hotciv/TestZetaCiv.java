package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.ZetaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestZetaCiv {

    Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new ZetaCivFactory());
    }

    @Test
    public void redShouldWinWhenAllCitiesAreConquered() {
        game.moveUnit(new Position(4, 3), new Position(4, 2));
        game.endOfTurn();
        game.endOfTurn();

        game.moveUnit(new Position(4, 2), new Position(4, 1));

        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void blueShouldWinAfter20RoundsWhen3AttacksHaveBeenWon() {

        //Getting player to round 21, since new rules for winning applies when there is <20 rounds
        for (int i = 0; i < 21; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }

        ((GameImpl) game).produceUnit(new Position(2, 5), Player.BLUE, GameConstants.LEGION);
        ((GameImpl) game).produceUnit(new Position(2, 6), Player.RED, GameConstants.LEGION);
        ((GameImpl) game).produceUnit(new Position(2, 7), Player.RED, GameConstants.LEGION);
        ((GameImpl) game).produceUnit(new Position(2, 8), Player.RED, GameConstants.LEGION);

        game.endOfTurn();
        assertThat(game.moveUnit(new Position(2, 5), new Position(2, 6)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.moveUnit(new Position(2, 6), new Position(2, 7)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.moveUnit(new Position(2, 7), new Position(2, 8)), is(true));
        game.endOfTurn();
        game.endOfTurn();

        assertThat(game.getWinner(), is(Player.BLUE));
    }

    @Test
    public void thereShouldBeNoWinner() {
        assertThat(game.getWinner(), is(nullValue()));
    }

    @Test
    public void owningAllCitiesCantWinTheGameAfter20Rounds() {
        for (int i = 0; i < 20; i++){
            game.endOfTurn();
            game.endOfTurn();
        }
        game.moveUnit(new Position(4, 3), new Position(4, 2));
        game.endOfTurn();
        game.endOfTurn();

        game.moveUnit(new Position(4, 2), new Position(4, 1));

        assertThat(game.getWinner(), is(nullValue()));

    }


}
