package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.DeltaCivFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestDeltaCiv {
    private Game game;

    @Before
    public void setUp(){
        game = new GameImpl(new DeltaCivFactory());
    }

    /**
     * Testing at random if the map is build correctly
     */
    @Test
    public void shouldBeAOceanOn00(){
        assertThat(game.getTileAt(new Position(0,0)).getTypeString(),is(GameConstants.OCEANS));
    }
    @Test
    public void shouldBeAOceanOn150(){
        assertThat(game.getTileAt(new Position(15,0)).getTypeString(),is(GameConstants.OCEANS));
    }
    @Test
    public void shouldBeAOceanOn016(){
        assertThat(game.getTileAt(new Position(0,15)).getTypeString(),is(GameConstants.OCEANS));
    }
    @Test
    public void shouldBeAOceanOn015(){
        assertThat(game.getTileAt(new Position(0,15)).getTypeString(),is(GameConstants.OCEANS));
    }
    @Test
    public void shouldBeAOceanOn68(){
        assertThat(game.getTileAt(new Position(6,8)).getTypeString(),is(GameConstants.OCEANS));
    }
    @Test
    public void shouldBeAMountainOn06(){
        assertThat(game.getTileAt(new Position(0,5)).getTypeString(),is(GameConstants.MOUNTAINS));
    }
    @Test
    public void shouldBeAHillsOn14(){
        assertThat(game.getTileAt(new Position(1,4)).getTypeString(),is(GameConstants.HILLS));
    }
    @Test
    public void shouldBeAHillsOn16(){
        assertThat(game.getTileAt(new Position(1,4)).getTypeString(),is(GameConstants.HILLS));
    }
    @Test
    public void shouldBeAPlainsOn77(){
        assertThat(game.getTileAt(new Position(7,7)).getTypeString(),is(GameConstants.PLAINS));
    }
    @Test
    public void shouldBeAPlainsOn157(){
        assertThat(game.getTileAt(new Position(15,8)).getTypeString(),is(GameConstants.PLAINS));
    }
    @Test
    public void shouldBeAPlainsOn08(){
        assertThat(game.getTileAt(new Position(0,8)).getTypeString(),is(GameConstants.PLAINS));
    }
    @Test
    public void shouldBeAForestOn39(){
        assertThat(game.getTileAt(new Position(1,9)).getTypeString(),is(GameConstants.FOREST));
    }
    @Test
    public void shouldBeAForestOn813(){
        assertThat(game.getTileAt(new Position(8,13)).getTypeString(),is(GameConstants.FOREST));
    }
    @Test
    public void shouldBeAForestOn910(){
        assertThat(game.getTileAt(new Position(9,10)).getTypeString(),is(GameConstants.FOREST));
    }
    @Test
    public void shouldBeBlueLegionOn44(){
        assertThat(game.getUnitAt(new Position(4,4)).getTypeString(),is(GameConstants.LEGION));
    }
    @Test
    public void shouldBeRedSettlerOn55(){
        assertThat(game.getUnitAt(new Position(5,5)).getTypeString(),is(GameConstants.SETTLER));
    }
}

