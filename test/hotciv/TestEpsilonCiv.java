package hotciv;

import hotciv.Strategies.AttackingStrategy.AttackingStrategy;
import hotciv.Strategies.AttackingStrategy.EpsilonCivAttackStrategy;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.EpsilonCivFactory;
import org.junit.Before;
import org.junit.Test;
import testStumps.FixedRandomNumbers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestEpsilonCiv {
    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new EpsilonCivFactory());
    }

    /**
     * Unit test
     * A lot of test for unit testing
     */
    @Test   //blue legion with two nearby friendly units should have an attacking strength of 4+1+1
    public void blueLegionShouldHaveAnAttackingStrengthOfSix(){
        ((GameImpl)game).produceUnit(new Position(2,5), Player.BLUE, GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.BLUE,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.BLUE,GameConstants.LEGION);

        AttackingStrategy as = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(((EpsilonCivAttackStrategy)as).calculateAttackingStrength(game,new Position(2,5)),is(4+1+1));
    }
    @Test   //When standing on a hill or forrest, multiplier should be 2
    public void multiplierShouldBeTwo(){
        ((GameImpl)game).createTile(new Position(2,5),GameConstants.HILLS);
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.getMultiplier(game,new Position(2,5)),is(2));
    }
    @Test   //When standing on a city, multiplier should be 3
    public void multiplierShouldBeThree(){
        ((GameImpl)game).buildNewCity(new Position(2,5),Player.RED);
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.getMultiplier(game,new Position(2,5)),is(3));
    }
    @Test   //When legion is places on a hill or forrest, attack strength should be boosted with a factor of 2
    public void redLegionAttackStrengthShouldBe12(){
        ((GameImpl)game).createTile(new Position(2,5),GameConstants.HILLS);
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.RED,GameConstants.LEGION);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.calculateAttackingStrength(game,new Position(2,5)),is((4+1+1)*2));
    }
    @Test   //With two nearby friendly units, legion defence should be 4
    public void redLegionDefenseStrengthShouldBe4(){
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.RED,GameConstants.LEGION);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.calculateDefensiveStrength(game,new Position(2,5)),is(2+1+1));
    }
    @Test   //With two nearby friendly units, and standing on a hill, defense strength should be (2+1+1)*2
    public void blueLegionDefenseStrengthShouldBe8(){
        ((GameImpl)game).createTile(new Position(2,5),GameConstants.HILLS);
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.RED,GameConstants.LEGION);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.calculateDefensiveStrength(game,new Position(2,5)),is((2+1+1)*2));

    }
    @Test   //getNrOfNearbyFriendlyUnits should return the number of friendly units in proximity
    public void blueArcherShouldHave3NearbyFriendlyUnits(){
        ((GameImpl)game).produceUnit(new Position(2,5),Player.BLUE,GameConstants.ARCHER);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.BLUE,GameConstants.SETTLER);
        ((GameImpl)game).produceUnit(new Position(2,4),Player.BLUE,GameConstants.SETTLER);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.BLUE,GameConstants.SETTLER);

        //Shouldn't count these units
        ((GameImpl)game).produceUnit(new Position(1,6),Player.RED,GameConstants.SETTLER);
        ((GameImpl)game).produceUnit(new Position(3,6),Player.RED,GameConstants.SETTLER);

        EpsilonCivAttackStrategy epsilonCivAttackStrategy = new EpsilonCivAttackStrategy(new FixedRandomNumbers());

        assertThat(epsilonCivAttackStrategy.getNrOfNearbyFriendlyUnits(game,new Position(2,5)),is(1+1+1));
    }
    @Test   //By placing legion on a hill, and placing 7 friendly units arround it, it's attack strength should reach (4+1+1+1+1+1+1)*2*random(6-1). This should be enough to defeat the blue legion.
    public void redLegionShouldDefeatBlueLegion(){
        ((GameImpl)game).createTile(new Position(2,5),GameConstants.HILLS);
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(2,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,4),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(3,6),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,5),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(1,6),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(2,6),Player.BLUE,GameConstants.LEGION);

        assertThat(game.moveUnit(new Position(2,5),new Position(2,6)),is(true));
    }
    /*@Test
    public void getAttacksShouldReturnTheNumberOfSuccessfulAttacks(){

        //Creating unit and buffing it
        ((GameImpl)game).produceUnit(new Position(2,5),Player.RED,GameConstants.LEGION);
        ((UnitImpl)game.getUnitAt(new Position(2,5))).setAttackingStrength(100);

        ((GameImpl)game).produceUnit(new Position(2,6),Player.BLUE,GameConstants.LEGION);
        game.moveUnit(new Position(2,5),new Position(2,6));
        game.endOfTurn();

        //Creating attacking unit and buffing it
        ((GameImpl)game).produceUnit(new Position(4,5),Player.BLUE,GameConstants.LEGION);
        ((UnitImpl)game.getUnitAt(new Position(4,5))).setAttackingStrength(100);

        ((GameImpl)game).produceUnit(new Position(4,6),Player.RED,GameConstants.LEGION);
        ((GameImpl)game).produceUnit(new Position(4,7),Player.RED,GameConstants.LEGION);
        game.moveUnit(new Position(4,5),new Position(4,6));
        game.endOfTurn();
        game.endOfTurn();

        game.moveUnit(new Position(4,6),new Position(4,7));

        //Red have defeated one foe, and should therefore have one win
        assertThat(((GameImpl) game).getAttackingStrategy().attacks.get(Player.RED),is(1));

        //Blue have defeated two foes, and should therefore have two wins
        assertThat(((GameImpl) game).getAttackingStrategy().attacks.get(Player.BLUE),is(2));
    }*/


}
