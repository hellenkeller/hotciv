package hotciv;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.gameTypeFactory.AlphaCivFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/** Created by Simon
 */
public class TestTranscriptTranscriptDecorator {
    private Game game, decoratee;
    private ByteArrayOutputStream outputStream;

    @Before
    public void setup(){
        decoratee = new GameImpl(new AlphaCivFactory());
        outputStream = new ByteArrayOutputStream();
        game = new TranscriptDecorator(decoratee, new PrintStream(outputStream));
    }

    @Test
    public void shouldPrintWhichPlayerThatEndsTurn(){
        assertThat(game.getPlayerInTurn(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
        game.endOfTurn();
        ArrayList<String> sl = getStringArray();
        assertThat(sl.get(0), is("RED ends turn."));
        assertThat(sl.get(1), is("BLUE ends turn."));
    }

    @Test
    public void moveUnitShouldPrintWhereUnitMovesToAndFrom(){
        assertThat(game.getPlayerInTurn(),is(Player.RED));
        game.moveUnit(new Position(2,0),new Position(3,0));

        ArrayList<String> s1 = getStringArray();
        assertThat(s1.get(0),is("RED moves archer from [2,0] to [3,0]"));
    }

    @Test
    public void changeWorkForceFocusShouldPrintPlayerAndCity(){
        game.changeWorkForceFocusInCityAt(new Position(1,1), GameConstants.foodFocus);

        ArrayList<String> s1 = getStringArray();

        assertThat(s1.get(0),is("RED changes work force fokus in his city at [1,1] to apple"));
    }

    @Test
    public void changeProductionShouldPrintPlayerAndTheNewProduction(){
        game.changeProductionInCityAt(new Position(1,1),GameConstants.ARCHER);

        ArrayList<String> s1 = getStringArray();

        assertThat(s1.get(0),is("RED changes production in a city at [1,1] to archer"));
    }

    @Test
    public void theToggleDecoratorMethodShouldToggleTheDecorator(){
        toggleDecorator();
        game.moveUnit(new Position(2,0),new Position(3,0));

        ArrayList<String> s1 = getStringArray();

        assertThat(s1.isEmpty(),is(true));
    }

    private ArrayList<String> getStringArray(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(outputStream.toByteArray())));
        ArrayList<String> stringList = new ArrayList<>();
        String line;
        try {
            while ((line = reader.readLine()) != null)
                stringList.add(line);
        } catch (IOException e){
            return stringList;
        }
        return stringList;
    }
    public void toggleDecorator(){
        if(game == decoratee){
            decoratee = game;
            game = new TranscriptDecorator(game,new PrintStream(outputStream));
        }else{
            game = decoratee;
        }
    }
}
