package testStumps;

import hotciv.Strategies.RandomNumberStrategy.RandomNumberStrategy;

public class FixedRandomNumbers implements RandomNumberStrategy {
    @Override
    public int[] generateNumbers() {
        int[]numbers = new int[2];

        numbers[0]=1;
        numbers[1]=1;

        return numbers;
    }
}
